/* Robert Higgins
 * Chess Bitmap Header
 */

#ifndef CHESS_BITMAP_H
#define CHESS_BITMAP_H

#include <stdbool.h>

bool Chess_Bitmaps_move(char *string, bool isWhite);
bool Chess_Bitmaps_is_checkmate(bool defender);
bool Chess_Bitmaps_is_check(bool defender);
bool Chess_Bitmaps_is_draw(bool defender);

void Chess_Bitmaps_debug_clear(void);
void Chess_Bitmaps_print_debug_info(void);
void Chess_Bitmaps_get_board(char *board);
void Chess_Bitmaps_initialize(void);

#endif
