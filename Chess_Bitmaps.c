/* Robert Higgins
 * Chess Bitmaps
 */

#include "Chess_Bitmaps.h"
#include <string.h>
#include <stdio.h>
#include <inttypes.h>

enum move_char {PIECE, S_FILE, S_RANK, CAPTURE, E_FILE, E_RANK, NOTE, PROMOTION};
enum array_names {WHITE_PIECES, WHITE_ATTACKS, BLACK_PIECES, BLACK_ATTACKS, EMPTY,
	KINGS, QUEENS, BISHOPS, KNIGHTS, ROOKS, PAWNS};
enum move_dir {UP_LEFT=1, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT};
enum castle_options {WQ, WK, BQ, BK};

bool set_en_pasant, did_en_pasant;
uint64_t bitmaps[11];
char en_pasant, can_castle[5], white_king[3], black_king[3];

bool is_piece(char piece, char file, char rank) {
	int p;
	switch (piece) {
	case 'P': p = PAWNS; break;
	case 'K': p = KINGS; break;
	case 'Q': p = QUEENS; break;
	case 'B': p = BISHOPS; break;
	case 'N': p = KNIGHTS; break;
	case 'R': p = ROOKS; break;
	default: return false;
	}

	return bitmaps[p] & 1LL << ((rank-'1')*8 + file-'a');
}
bool is_side(bool isWhite, char file, char rank) {return bitmaps[isWhite ? WHITE_PIECES : BLACK_PIECES] & 1LL << ((rank-'1')*8 + file-'a');}
bool is_side_and_piece(bool isWhite, char piece, char file, char rank) {return is_side(isWhite, file, rank) && is_piece(piece, file, rank);}
bool is_empty(char file, char rank) {return bitmaps[EMPTY] & 1LL << ((rank-'1')*8 + file-'a');}
bool can_attack(bool isWhite, char file, char rank) {return bitmaps[isWhite ? WHITE_ATTACKS : BLACK_ATTACKS] & 1LL << ((rank-'1')*8 + file-'a');}

char get_side(char file, char rank) {
	if (is_side(true, file, rank)) return 'W';
	else if (is_side(false, file, rank)) return 'B';
	else return '\0';
}

char get_piece(char file, char rank) {
	for (int a=KINGS; a<=PAWNS; a++) {
		if (bitmaps[a] & 1LL << ((rank-'1')*8 + file-'a')) {
			switch (a) {
			case KINGS: return 'K';
			case QUEENS: return 'Q';
			case BISHOPS: return 'B';
			case KNIGHTS: return 'N';
			case ROOKS: return 'R';
			case PAWNS: return 'P';
			}
		}
	}

	return '\0';
}

void get_piece_name(char *piece, char file, char rank) {
	char *empty = ((file - 'a') % 2 == (rank - '1') % 2) ? "\xB1\xB1" : "  ";

	if (is_empty(file, rank)) {strcpy(piece, empty); return;}
	piece[0] = get_side(file, rank); if (!piece[0]) {strcpy(piece, "ER\0"); return;}
	piece[1] = get_piece(file, rank); if (!piece[1]) {strcpy(piece, "ER\0"); return;}
	piece[2] = '\0';
}

enum move_dir get_direction(char sFile, char sRank, char eFile, char eRank) {
	if (eFile < sFile && eRank > sRank) return UP_LEFT;
	else if (eFile == sFile && eRank > sRank) return UP;
	else if (eFile > sFile && eRank > sRank) return UP_RIGHT;
	else if (eFile > sFile && eRank == sRank) return RIGHT;
	else if (eFile > sFile && eRank < sRank) return DOWN_RIGHT;
	else if (eFile == sFile && eRank < sRank) return DOWN;
	else if (eFile < sFile && eRank < sRank) return DOWN_LEFT;
	else if (eFile < sFile && eRank == sRank) return LEFT;
	else return -1;
}

void get_adjacent(enum move_dir dir, char *file, char *rank) {
	switch (dir) {
	case UP_LEFT: (*file)--; case UP: (*rank)++; break;
	case UP_RIGHT: (*rank)++; case RIGHT: (*file)++; break;
	case DOWN_RIGHT: (*file)++; case DOWN: (*rank)--; break;
	case DOWN_LEFT: (*rank)--; case LEFT: (*file)--; break;
	default: file = '\0'; rank = '\0'; return;
	}

	if (*file < 'a' || *file > 'h') *file = '\0';
	if (*rank < '1' || *rank > '8') *rank = '\0';
}

void search_in_direction(char *move, bool side, enum move_dir dir, char file, char rank) {
	get_adjacent(dir, &file, &rank);

	if (!(file && rank) || is_side(!side, file, rank)) {
		move[PIECE] = '-';
		move[S_FILE] = '-';
		move[S_RANK] = '-';
		return;
	}

	if (is_empty(file, rank)) {search_in_direction(move, side, dir, file, rank); return;}

	move[PIECE] = get_piece(file, rank);
	move[S_FILE] = file;
	move[S_RANK] = rank;
}

void set_attack(bool side, char file, char rank) {bitmaps[side ? WHITE_ATTACKS : BLACK_ATTACKS] |=  1LL << ((rank-'1')*8 + file-'a');}

void set_attacks_in_line(bool side, enum move_dir dir, char file, char rank) {
	get_adjacent(dir, &file, &rank);
	if (!(file && rank)) return;

	set_attack(side, file, rank);

	if (is_piece('K', file, rank)) {
		get_adjacent(dir, &file, &rank);
		if (file && rank) set_attack(side, file, rank);
		return;
	}

	if (!is_empty(file, rank)) return;

	set_attacks_in_line(side, dir, file, rank);
}

void calculate_attacks() {
	for (int a=0; a<8; a++) {
		bitmaps[WHITE_ATTACKS] = 0;
		bitmaps[BLACK_ATTACKS] = 0;
	}

	char piece[3];
	bool side;

	for (char file = 'a'; file <= 'h'; file++) {
		for (char rank = '1'; rank <= '8'; rank++) {
			if (is_empty(file, rank)) continue;

			get_piece_name(piece, file, rank);
			side = (piece[0] == 'W');
			switch (piece[1]) {
			case 'P': {
				int dir = (side ? 1 : -1);
				if (file-1 >= 'a') set_attack(side, file-1, rank+dir);
				if (file+1 <= 'h') set_attack(side, file+1, rank+dir);
				break;
			}
			case 'K':
				for (int dir = UP_LEFT; dir <= LEFT; dir++) {
					char f = file, r = rank; get_adjacent(dir, &f, &r);
					if (!(f&&r)) continue;
					set_attack(side, f, r);
				}
				break;
			case 'N':
				for (int f=-2; f<=2; f++) {
					if (f==0) continue;
					for (int r=-2; r<=2; r++) {
						if (r == 0 || f+r == 0 || f-r == 0 ||
								file+f < 'a' || file+f > 'h' || rank+r < '1' || rank+r > '8') continue;
						set_attack(side, file+f, rank+r);
					}
				}
				break;
			case 'Q': for (int dir = UP_LEFT; dir <= LEFT; dir++) set_attacks_in_line(side, dir, file, rank); break;
			case 'B': for (int dir = UP_LEFT; dir <= DOWN_LEFT; dir+=2) set_attacks_in_line(side, dir, file, rank); break;
			case 'R': for (int dir = UP; dir <= LEFT; dir+=2) set_attacks_in_line(side, dir, file, rank); break;
			}
		}
	}
}

void en_pasant_capture(bool isWhite) {
	bitmaps[isWhite ? BLACK_PIECES : WHITE_PIECES] &= ~(1LL << ((isWhite ? 4 : 3)*8 + en_pasant - 'a'));
	bitmaps[PAWNS] &= ~(1LL << ((isWhite ? 4 : 3)*8 + en_pasant - 'a'));
	bitmaps[EMPTY] |= 1LL << ((isWhite ? 4 : 3)*8 + en_pasant - 'a');
	did_en_pasant = true;
}

void clear_piece(char file, char rank) {
	bitmaps[WHITE_PIECES] &= ~(1LL << ((rank-'1')*8 + file-'a'));
	bitmaps[BLACK_PIECES] &= ~(1LL << ((rank-'1')*8 + file-'a'));
	bitmaps[EMPTY] |= 1LL << ((rank-'1')*8 + file-'a');

	for (int a=KINGS; a<=PAWNS; a++) bitmaps[a] &= ~(1LL << ((rank-'1')*8 + file-'a'));
}

void spawn_piece(bool isWhite, char piece, char file, char rank) {
	if (!piece) return;

	bitmaps[isWhite ? WHITE_PIECES : BLACK_PIECES] |= 1LL << ((rank-'1')*8 + file-'a');
	bitmaps[isWhite ? BLACK_PIECES : WHITE_PIECES] &= ~(1LL << ((rank-'1')*8 + file-'a'));
	bitmaps[EMPTY] &= ~(1LL << ((rank-'1')*8 + file-'a'));

	int a; for (a=KINGS; a<=PAWNS; a++) bitmaps[a] &= ~(1LL << ((rank-'1')*8 + file-'a'));

	switch (piece) {
	case 'P': a = PAWNS; break;
	case 'K': a = KINGS; break;
	case 'Q': a = QUEENS; break;
	case 'B': a = BISHOPS; break;
	case 'N': a = KNIGHTS; break;
	case 'R': a = ROOKS; break;
	}

	bitmaps[a] |= 1LL << ((rank-'1')*8 + file-'a');
}

void make_move(bool isWhite, char piece, char sFile, char sRank, char eFile, char eRank) {
	clear_piece(sFile, sRank);
	spawn_piece(isWhite, piece, eFile, eRank);
	calculate_attacks();
}

bool test_move(bool isWhite, char piece, char sFile, char sRank, char eFile, char eRank, bool autoUndo) {
	char captured_piece = get_piece(eFile, eRank);

	make_move(isWhite, piece, sFile, sRank, eFile, eRank);

	bool safe = !Chess_Bitmaps_is_check(isWhite);

	if (!safe || autoUndo) {
		make_move(isWhite, piece, eFile, eRank, sFile, sRank);
		if (did_en_pasant) spawn_piece(!isWhite, 'P', eFile, isWhite ? '5' : '4');
		else spawn_piece(!isWhite, captured_piece, eFile, eRank);
		return safe;
	}

	did_en_pasant = false;

	if (set_en_pasant) {
		en_pasant = eFile;
		set_en_pasant = false;
	} else en_pasant = '-';

	return safe;
}

bool can_move_in_line(bool side, char piece, enum move_dir dir, char file, char rank) {
	char f=file, r=rank; get_adjacent(dir, &f, &r);

	while (f && r) {
		if (is_side(side, f, r)) return false;
		if (test_move(side, piece, file, rank, f, r, true)) return true;
		if (is_side(!side, f, r)) return false;
		get_adjacent(dir, &f, &r);
	}

	return false;
}

bool move_pawn(char *move, bool isWhite) {
	char dir = isWhite ? 1 : -1;

	if (move[E_RANK] == (isWhite ? '8' : '1')) {
		if (move[NOTE] == '=') {switch (move[PROMOTION]) {case 'Q': case 'N': case 'R': case 'B': break; default: return false;}
		} else return false;
	}
	if (move[S_FILE] != move[E_FILE] && move[CAPTURE] != 'x') return false;
	if (move[S_RANK] != move[E_RANK] && move[S_RANK] != '-' &&
			(!is_side_and_piece(isWhite, 'P', move[S_FILE], move[S_RANK]))) {
		return false;
	}

	if (move[CAPTURE] == '-') {
		if (is_side_and_piece(isWhite, 'P', move[E_FILE], move[E_RANK]-dir)) {
			bool canMove = test_move(isWhite, 'P', move[E_FILE], move[E_RANK]-dir, move[E_FILE], move[E_RANK], false);
			if (move[E_RANK] == (isWhite ? '8' : '1')) spawn_piece(isWhite, move[PROMOTION], move[E_FILE], move[E_RANK]);
			return canMove;
		} else if (move[E_RANK] == (isWhite ? '4' : '5') && is_empty(move[E_FILE], move[E_RANK]-dir) &&
			       	is_side_and_piece(isWhite, 'P', move[E_FILE], move[E_RANK]-dir*2)) {
			set_en_pasant = true;
			return test_move(isWhite, 'P', move[E_FILE], move[E_RANK]-dir*2, move[E_FILE], move[E_RANK], false);
		} else return false;
	} else if (move[CAPTURE] == 'x') {
		if (is_empty(move[E_FILE], move[E_RANK])) {
			if (move[E_FILE] == en_pasant && move[E_RANK] == (isWhite ? '6' : '3')) {
				if (is_side_and_piece(isWhite, 'P', move[S_FILE], move[E_RANK]-dir)) {
					en_pasant_capture(isWhite);
					return test_move(isWhite, 'P', move[S_FILE], move[E_RANK]-dir, move[E_FILE], move[E_RANK], false);
				} else return false;
			} else return false;
		} else if (is_side(!isWhite, move[E_FILE], move[E_RANK])) {
			if (is_side_and_piece(isWhite, 'P', move[S_FILE], move[E_RANK]-dir)) {
				return test_move(isWhite, 'P', move[S_FILE], move[E_RANK]-dir, move[E_FILE], move[E_RANK], false);
			} else return false;
		} else return false;
	}

	return false;
}

bool move_king(char *move, bool isWhite) {
	if (can_attack(!isWhite, move[E_FILE], move[E_RANK])) return false;

	char f, r;
	for (int dir=UP_LEFT; dir<=LEFT; dir++) {
		f = move[E_FILE]; r = move[E_RANK]; get_adjacent(dir, &f, &r);

		if (!(f&&r)) continue;

		if (is_side_and_piece(isWhite, 'K', f, r)) {
			if (isWhite) {white_king[0] = move[E_FILE]; white_king[1] = move[E_RANK];}
			else {black_king[0] = move[E_FILE]; black_king[1] = move[E_RANK];}
			can_castle[isWhite ? WQ : BQ] = '\0'; can_castle[isWhite ? WK : BK] = '\0';
			make_move(isWhite, 'K', f, r, move[E_FILE], move[E_RANK]);
			return true;
		}
	}

	return false;
}

bool move_queen(char *move, bool isWhite) {
	char search[] = "---\0", search2[] = "---\0";
	int dirs[] = {UP_LEFT, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, 0};

	if (move[S_FILE] == move[E_FILE] && move[S_RANK] == move[E_RANK]) {}
	else if (move[S_RANK] == '-') {
		if (move[E_FILE] > move[S_FILE]) {dirs[0] = UP_LEFT; dirs[1] = LEFT; dirs[2] = DOWN_LEFT; dirs[3] = 0;}
		else {dirs[0] = UP_RIGHT; dirs[1] = RIGHT; dirs[2] = DOWN_RIGHT; dirs[3] = 0;}
	} else if (move[S_FILE] == '-') {
		if (move[E_RANK] > move[S_RANK]) {dirs[0] = DOWN_RIGHT; dirs[1] = DOWN; dirs[2] = DOWN_LEFT; dirs[3] = 0;}
		else {dirs[0] = UP_LEFT; dirs[1] = UP; dirs[2] = UP_RIGHT; dirs[3] = 0;}
	} else {
		int dir = get_direction(move[E_FILE], move[E_RANK], move[S_FILE], move[S_RANK]);
		if (dir == -1) return false;
		dirs[0] = dir; dirs[1] = 0;
	}

	for (int a=0; dirs[a]; a++) {
		search_in_direction(search, isWhite, dirs[a], move[E_FILE], move[E_RANK]);
		if (search[PIECE] == 'Q') {
			if (search2[PIECE] == '-') strcpy(search2, search);
			else return false;
		}
	}

	if (search2[PIECE] == '-') return false;

	return test_move(isWhite, 'Q', search2[S_FILE], search2[S_RANK], move[E_FILE], move[E_RANK], false);
}

bool move_bishop(char *move, bool isWhite) {
	char search[] = "---\0", search2[] = "---\0";
	int dirs[] = {UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT, 0};

	if (move[S_FILE] == move[E_FILE] && move[S_RANK] == move[E_RANK]) {}
	else if (move[S_RANK] == '-') {
		if (move[E_FILE] > move[S_FILE]) {dirs[0] = UP_LEFT; dirs[1] = DOWN_LEFT; dirs[2] = 0;}
		else {dirs[0] = UP_RIGHT; dirs[1] = DOWN_RIGHT; dirs[2] = 0;}
	} else if (move[S_FILE] == '-') {
		if (move[E_RANK] > move[S_RANK]) {dirs[0] = DOWN_LEFT; dirs[1] = DOWN_RIGHT; dirs[2] = 0;}
		else {dirs[0] = UP_LEFT; dirs[1] = UP_RIGHT; dirs[2] = 0;}
	} else {
		enum move_dir dir = get_direction(move[E_FILE], move[E_RANK], move[S_FILE], move[S_RANK]);
		if (dir == -1 || dir % 2 == 0) return false;
		dirs[0] = dir; dirs[1] = 0;
	}

	for (int a=0; dirs[a]; a++) {
		search_in_direction(search, isWhite, dirs[a], move[E_FILE], move[E_RANK]);
		if (search[PIECE] == 'B') {
			if (search2[PIECE] == '-') strcpy(search2, search);
			else return false;
		}
	}

	if (search2[PIECE] == '-') return false;

	return test_move(isWhite, 'B', search2[S_FILE], search2[S_RANK], move[E_FILE], move[E_RANK], false);
}

bool move_knight(char *move, bool isWhite) {
	char file, rank, search[] = "---\0";

	for(int a=-2;a<=2;a++) {
		if(a == 0) continue;
		for(int b=-2;b<=2;b++) {
			if (b == 0 || a-b == 0 || a+b == 0) continue;

			file = move[E_FILE]+a; rank = move[E_RANK]+b;
			if ((file < 'a' || file > 'h' || rank < '1' || rank > '8')) continue;
			if (move[S_FILE] == '-' && move[S_RANK] == '-') {}
			else if ((move[S_RANK] == '-' && move[S_FILE] != file) ||
				(move[S_FILE] == '-' && move[S_RANK] != rank)) continue;

			if (is_side_and_piece(isWhite, 'N', file, rank)) {
				if (search[PIECE] == '-') {search[PIECE] = 'N'; search[S_FILE] = file; search[S_RANK] = rank;}
				else return false;
			}
		}
	}

	if (search[PIECE] == '-') return false;

	return test_move(isWhite, 'N', search[S_FILE], search[S_RANK], move[E_FILE], move[E_RANK], false);
}

bool move_rook(char *move, bool isWhite) {
	char search[] = "---\0", search2[] = "---\0";
	int dirs[] = {UP, DOWN, LEFT, RIGHT, 0};

	if (move[S_FILE] == move[E_FILE] && move[S_RANK] == move[E_RANK]) {}
	else if (move[S_RANK] == '-') {dirs[0] = UP; dirs[1] = DOWN; dirs[2] = 0;}
	else if (move[S_FILE] == '-') {dirs[0] = LEFT; dirs[1] = RIGHT; dirs[2] = 0;}
	else {
		enum move_dir dir = get_direction(move[E_FILE], move[E_RANK], move[S_FILE], move[S_RANK]);
		if (dir == -1 || dir % 2 == 1) return false;
		dirs[0] = dir; dirs[1] = 0;
	}

	for (int a=0; dirs[a]; a++) {
		search_in_direction(search, isWhite, dirs[a], move[E_FILE], move[E_RANK]);
		if (search[PIECE] == 'R') {
			if (search2[PIECE] == '-') strcpy(search2, search);
			else return false;
		}
	}

	if (search2[PIECE] == '-') return false;

	if (search2[S_RANK] == (isWhite ? '1' : '8')) {
		if (search2[S_FILE] == 'a') can_castle[isWhite ? WQ : BQ] = '\0';
		else if (search2[S_FILE] == 'h') can_castle[isWhite ? WK : BK] = '\0';
	}
	return test_move(isWhite, 'R', search2[S_FILE], search2[S_RANK], move[E_FILE], move[E_RANK], false);
}

bool castle(char *move, bool isWhite) {
	char rank = isWhite ? '1' : '8';

	if ((!strcmp(move, "0-0-0") || !strcmp(move, "O-O-O")) && (can_castle[isWhite ? WQ : BQ])) { //Queen side
		if (is_empty('b', rank) && is_empty('c', rank) && is_empty('d', rank) &&
				!can_attack(!isWhite, 'c', rank) && !can_attack(!isWhite, 'd', rank) && !can_attack(!isWhite, 'e', rank)) {
			make_move(isWhite, 'K', 'e', rank, 'c', rank);
			make_move(isWhite, 'R', 'a', rank, 'd', rank);

			if (isWhite) {white_king[0] = 'c'; white_king[1] = rank;}
			else {black_king[0] = 'c'; black_king[1] = rank;}

			en_pasant = '-';

			return true;
		}
	} else if ((!strcmp(move, "0-0") || !strcmp(move, "O-O")) && (can_castle[isWhite ? WK : BK])) { //King side
		if (is_empty('f', rank) && is_empty('g', rank) &&
				!can_attack(!isWhite, 'e', rank) && !can_attack(!isWhite, 'f', rank) && !can_attack(!isWhite, 'g', rank)) {
			make_move(isWhite, 'K', 'e', rank, 'g', rank);
			make_move(isWhite, 'R', 'h', rank, 'f', rank);

			if (isWhite) {white_king[0] = 'g'; white_king[1] = rank;}
			else {black_king[0] = 'g'; black_king[1] = rank;}

			en_pasant = '-';

			return true;
		}
	} else return false;

	return false;
}

bool choose_move(char *move, bool side) {
	if (move[E_FILE] == '-' && move[E_RANK] == '-') {move[E_FILE] = move[S_FILE]; move[E_RANK] = move[S_RANK];}
	else if (move[S_FILE] == '-' && move[S_RANK] == '-') {move[S_FILE] = move[E_FILE]; move[S_RANK] = move[E_RANK];}

	if (is_side(side, move[E_FILE], move[E_RANK])) return false;
	if (move[CAPTURE] == 'x' && move[PIECE] != 'P' && !is_side(!side, move[E_FILE], move[E_RANK])) return false;
	else if (move[CAPTURE] == '-' && !is_empty(move[E_FILE], move[E_RANK])) return false;

	switch (move[PIECE]) {
	case 'P': return move_pawn(move, side);
	case 'K': return move_king(move, side);
	case 'Q': return move_queen(move, side);
	case 'R': return move_rook(move, side);
	case 'B': return move_bishop(move, side);
	case 'N': return move_knight(move, side);
	default: return false;
	}
}

void find_directional_attackers(char *attackers, int *index, bool attacker, char file, char rank) {
	char f, r, p;
	for (int dir = UP_LEFT; dir <= LEFT; dir++) {
		f = file; r = rank; get_adjacent(dir, &f, &r);
		if (!(f&&r) || is_side(!attacker, f, r)) continue;

		if (is_side_and_piece(attacker, 'P', f, r) &&
				((!attacker && (dir == UP_LEFT || dir == UP_RIGHT)) ||
				(attacker && (dir == DOWN_LEFT || dir == DOWN_RIGHT)))) {
			attackers[(*index)++] = f;
			attackers[(*index)++] = r;
			continue;
		}

		while ((f&&r) && is_empty(f,r)) get_adjacent(dir, &f, &r);

		if ((f&&r) && is_side(attacker, f, r)) {
			p = get_piece(f,r);
			switch (p) {
			case 'Q': attackers[(*index)++] = f; attackers[(*index)++] = r; break;
			case 'B': if (dir % 2 == 1) {attackers[(*index)++] = f; attackers[(*index)++] = r;} break;
			case 'R': if (dir % 2 == 0) {attackers[(*index)++] = f; attackers[(*index)++] = r;} break;
			}
		}
	}
}

void find_knight_attackers(char *attackers, int *index, bool attacker, char file, char rank) {
	for (int f = -2; f <= 2; f++) {
		if (f==0) continue;
		for (int r = -2; r <= 2; r++) {
			if (r == 0 || f+r == 0 || f-r == 0 ||
					file+f < 'a' || file+f > 'h' || rank+r < '1' || rank+r > '8') continue;
			if (is_side_and_piece(attacker, 'N', file+f, rank+r)) {
				attackers[(*index)++] = file+f;
				attackers[(*index)++] = rank+r;
			}
		}
	}
}

int find_attackers(char *attackers, bool attacker, char file, char rank) {
	int index = 0;
	find_knight_attackers(attackers, &index, attacker, file, rank);
	find_directional_attackers(attackers, &index, attacker, file, rank);
	attackers[index] = '\0';
	return index/2;
}

bool can_defend(char *kingAttacker, bool defender) {
	char attackers[100], file = kingAttacker[0], rank = kingAttacker[1], p;
	enum move_dir dir = get_direction(file, rank, defender ? white_king[0] : black_king[0], defender ? white_king[1] : black_king[1]);
	int numAttackers, pawn_dir = defender ? 1 : -1;

	if (en_pasant != '-') {
		char ep = defender ? '5' : '4';
		if (en_pasant > 'a' && is_side_and_piece(defender, 'P', en_pasant-1, ep)) {
			en_pasant_capture(defender);
			if (test_move(defender, 'P', en_pasant-1, ep, en_pasant, ep+pawn_dir, true)) return true;
		}

		if (en_pasant < 'h' && is_side_and_piece(defender, 'P', en_pasant+1, ep)) {
			en_pasant_capture(defender);
			if (test_move(defender, 'P', en_pasant+1, ep, en_pasant, ep+pawn_dir, true)) return true;
		}
	}

	for (;!is_side_and_piece(defender, 'K', file, rank); get_adjacent(dir, &file, &rank)) {
		if (is_empty(file, rank)) {
			if (is_side_and_piece(defender, 'P', file, rank-pawn_dir) &&
					test_move(defender, 'P', file, rank-pawn_dir, file, rank, true))
				return true;
			if (rank == defender ? '4' : '5' && is_side_and_piece(defender, 'P', file, rank-pawn_dir*2) &&
					is_empty(file, rank-pawn_dir) && test_move(defender, 'P', file, rank-pawn_dir, file, rank, true))
				return true;
		}

		if (!can_attack(defender, file, rank)) {
			if (is_piece('N', file, rank)) return false;
			else continue;
		}

		numAttackers = find_attackers(attackers, defender, file, rank);
		for (int a=0; a<numAttackers; a++) {
			p = get_piece(attackers[a*2], attackers[a*2+1]);
			if (p == 'P' && is_empty(file, rank)) continue;
			if (test_move(defender, p, attackers[a*2], attackers[a*2+1], file, rank, true)) return true;
		}
	}

	return false;
}

bool debug_clear(char *string) {
	char file = string[1], rank = string[2];
	if (get_piece(file, rank) == 'K' || file < 'a' || file > 'h' || rank < '1' || rank > '8') return false;
	clear_piece(file, rank);
	calculate_attacks();
	return true;
}

bool debug_spawn(char *string) {
	char side = string[1], piece = string[2], file = string[3], rank = string[4];
	if (get_piece(file, rank) == 'K' ||	(side != 'W' && side != 'B')
			|| file < 'a' || file > 'h' || rank < '1' || rank > '8') return false;
	switch (piece) {case 'K': case 'Q': case 'R': case 'B': case 'N': case 'P': break; default: return false;}
	bool white = side == 'W';

	if (piece == 'K') {
		if (white) {
			clear_piece(white_king[0], white_king[1]);
			white_king[0] = file; white_king[1] = rank;
		} else {
			clear_piece(black_king[0], black_king[1]);
			black_king[0] = file; black_king[1] = rank;
		}
	}

	clear_piece(file, rank);
	spawn_piece(white, piece, file, rank);
	calculate_attacks();
	return true;
}

void Chess_Bitmaps_initialize() {
	for (int a=WHITE_PIECES; a<=PAWNS; a++) {
		bitmaps[a] = 0;
	}

	set_en_pasant = false; did_en_pasant = false;
	en_pasant = '-'; strcpy(can_castle, "AHah\0"); strcpy(white_king, "e1\0"); strcpy(black_king, "e8\0");

	bitmaps[WHITE_PIECES] = 0x000000000000ffffLL;
	bitmaps[BLACK_PIECES] = 0xffff000000000000LL;
	bitmaps[EMPTY] = 0x0000ffffffff0000LL;
	bitmaps[PAWNS] = 0x00ff00000000ff00LL;
	bitmaps[KNIGHTS] = 0x4200000000000042LL;
	bitmaps[BISHOPS] = 0x2400000000000024LL;
	bitmaps[ROOKS] = 0x8100000000000081LL;
	bitmaps[QUEENS] = 0x0800000000000008LL;
	bitmaps[KINGS] = 0x1000000000000010LL;

	calculate_attacks();
}

void Chess_Bitmaps_debug_clear(void) {
	for (char f = 'a'; f <= 'h'; f++) {
		for (char r = '1'; r <= '8'; r++) {
			if (get_piece(f, r) != 'K') clear_piece(f,r);
		}
	}

	calculate_attacks();
}

void Chess_Bitmaps_print_debug_info() {
	for (int a=WHITE_PIECES; a<=PAWNS; a++) {
		printf("bitmaps[%x] = %016" PRIx64 "\n", a, bitmaps[a]);
	}

	char board[1000],
		line[] = "+--+--+--+--+--+--+--+--+\n",
		newLine[] = "|  |  |  |  |  |  |  |  |\n";

	//white and black pieces
	strcpy(board, line);

	for (char rank = '8'; rank>='1'; rank--) {
		int index = 1;
		for (char file='a'; file<='h'; file++) {
			newLine[index++] = is_side(true, file, rank) ? 'W' : ' ';
			newLine[index++] = is_side(false, file, rank) ? 'B' : ' ';
			index++;
		}
		strcat(board, newLine);
		strcat(board, line);
	}

	printf("\nPieces:\n%s\n\n", board);

	//empty
	strcpy(board, line);

	for (char rank = '8'; rank>='1'; rank--) {
		int index = 1;
		for (char file='a'; file<='h'; file++) {
			newLine[index++] = is_empty(file, rank) ? 'E' : ' ';
			index+=2;
		}
		strcat(board, newLine);
		strcat(board, line);
	}

	printf("Empty:\n%s\n", board);

	//white and black attacks
	strcpy(board, line);

	for (char rank = '8'; rank>='1'; rank--) {
		int index = 1;
		for (char file='a'; file<='h'; file++) {
			newLine[index++] = can_attack(true, file, rank) ? 'W' : ' ';
			newLine[index++] = can_attack(false, file, rank) ? 'B' : ' ';
			index++;
		}
		strcat(board, newLine);
		strcat(board, line);
	}

	printf("Attacks:\n%s\n\n", board);

	//pieces
	char *names[] = {"KINGS", "QUEENS", "BISHOPS", "KNIGHTS", "ROOKS", "PAWNS"},
			n[] = {'K', 'Q', 'B', 'N', 'R', 'P'};

	for (int a = KINGS; a <= PAWNS; a++) {
		strcpy(board, line);

		for (char rank = '8'; rank>='1'; rank--) {
			int index = 1;
			for (char file='a'; file<='h'; file++) {
				newLine[index++] = is_piece(n[a-KINGS], file, rank) ? n[a-KINGS] : ' ';
				newLine[index++] = ' ';
				index++;
			}
			strcat(board, newLine);
			strcat(board, line);
		}

		printf("%s:\n%s\n\n", names[a-KINGS], board);
	}

	printf("set_en_pasant = %s, did_en_pasant = %s\n"
			"en_pasant = '%c', can_castle = %s, white_king = %s, black_king = %s\n",
			set_en_pasant ? "true" : "false", did_en_pasant ? "true" : "false",
			en_pasant, can_castle, white_king, black_king);
}

void Chess_Bitmaps_get_board(char *board) {
	char line[] = "  +--+--+--+--+--+--+--+--+\n",
			newLine[] = "  |  |  |  |  |  |  |  |  |\n";

	strcpy(board, line);

	char piece[3];

	for (char rank = '8'; rank>='1'; rank--) {
		newLine[0] = rank;
		int index = 3;
		for (char file='a'; file<='h'; file++) {
			get_piece_name(piece, file, rank);
			newLine[index++] = piece[0];
			newLine[index++] = piece[1];
			index++;
		}
		strcat(board, newLine);
		strcat(board, line);
	}

	strcat(board, "    a  b  c  d  e  f  g  h\n");
}

bool Chess_Bitmaps_is_checkmate(bool defender) {
	char file = defender ? white_king[0] : black_king[0], rank = defender ? white_king[1] : black_king[1];

	for (int dir = UP_LEFT; dir <= LEFT; dir++) {
		char f = file, r = rank; get_adjacent(dir, &f, &r);
		if (!(f&&r)) continue;
		if (!is_side(defender, f,r) && !can_attack(!defender, f,r)) return false;
	}

	char attackers[10];
	int numAttackers = find_attackers(attackers, !defender, file, rank);
	if (numAttackers >= 2) return true;
	else if (numAttackers == 0) return false;

	return !can_defend(attackers, defender);
}

bool Chess_Bitmaps_is_check(bool defender) {return can_attack(!defender, defender ? white_king[0] : black_king[0], defender ? white_king[1] : black_king[1]);}

bool Chess_Bitmaps_is_draw(bool defender) {
	for (char file = 'a'; file <= 'h'; file++) {
		for (char rank = '1'; rank <= '8'; rank++) {
			if (is_side(defender, file, rank)) {
				char p = get_piece(file,rank);
				switch (p) {
				case 'P': {
					char dir = defender ? 1 : -1;
					if ((is_empty(file, rank+dir) && test_move(defender, 'P', file, rank, file, rank+dir, true)) ||
							(rank == defender ? '2' : '7' && is_empty(file, rank+dir) && is_empty(file, rank+dir*2) &&
									test_move(defender, 'P', file, rank, file, rank+dir*2, true)) ||
							(file > 'a' && is_side(!defender, file-1,rank+dir) && test_move(defender, 'P', file, rank, file-1, rank+dir, true)) ||
							(file < 'h' && is_side(!defender, file+1,rank+dir) && test_move(defender, 'P', file, rank, file+1, rank+dir, true)))
						return false;
					if (en_pasant != '-' && rank == (defender ? '5' : '4') && (file-en_pasant == 1 || file-en_pasant == -1)) {
						en_pasant_capture(defender);
						if (test_move(defender, 'P', file, rank, en_pasant, rank+dir, true)) return false;
					}
					break;
				}
				case 'K':
					for (int dir=UP_LEFT; dir <= LEFT; dir++) {
						char f=file, r=rank; get_adjacent(dir,&f,&r);
						if(!(f&&r)) continue;
						if (!can_attack(!defender, f, r)) return false;
					}
					break;
				case 'N':
					for (int f=-2; f<=2; f++) {
						if (f==0) continue;
						for (int r=-2; r<=2; r++) {
							if (r == 0 || f + r == 0 || f - r == 0 || is_side(defender, file+f, rank+r) ||
									file+f < 'a' || file+f > 'h' || rank+r < '1' || rank+r > '8') continue;
							if (test_move(defender, 'N', file, rank, file+f, rank+r, true)) return false;
						}
					}
					break;
				case 'Q':
					for (int dir=UP_LEFT; dir <= LEFT; dir++)
						if (can_move_in_line(defender, 'Q', dir, file, rank)) return false;
					break;
				case 'B':
					for (int dir=UP_LEFT; dir <= DOWN_LEFT; dir+=2)
						if (can_move_in_line(defender, 'B', dir, file, rank)) return false;
					break;
				case 'R':
					for (int dir=UP; dir <= LEFT; dir+=2)
						if (can_move_in_line(defender, 'R', dir, file, rank)) return false;
					break;
				}
			}
		}
	}

	return true;
}

bool Chess_Bitmaps_move(char *string, bool isWhite) {
	char move[] = "--------\0"; int index = 0;

	switch (string[index]) {
	case '\0': return false;
	case '!':
		if (string[1] == 'W' || string[1] == 'B') return debug_spawn(string);
		else return debug_clear(string);
	case 'O': case '0': return castle(string, isWhite);
	case 'P': case 'K': case 'Q': case 'R': case 'B': case 'N':
		move[PIECE] = string[index++]; break;
	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h':
		move[PIECE] = 'P'; break;
	default: return false;
	}

	if (string[index] == '\0') return false;
	else if (string[index] >= 'a' && string[index] <= 'h')
		move[S_FILE] = string[index++];
	else move[S_FILE] = '-';

	if (string[index] == '\0') return false;
	else if (string[index] >= '1' && string[index] <= '8')
		move[S_RANK] = string[index++];
	else move[S_RANK] = '-';

	switch (string[index]) {
	case '\0': return choose_move(move, isWhite);
	case 'x': move[CAPTURE] = string[index++]; break;
	default: move[CAPTURE] = '-'; break;
	}

	if (string[index] == '\0') return false;
	else if (string[index] >= 'a' && string[index] <= 'h')
		move[E_FILE] = string[index++];
	else move[E_FILE] = '-';

	if (string[index] == '\0') return false;
	else if (string[index] >= '1' && string[index] <= '8')
		move[E_RANK] = string[index++];
	else move[E_RANK] = '-';

	switch (string[index]) {
	case '\0': case '+': case '#': return choose_move(move, isWhite);
	case '=': move[NOTE] = string[index++]; break;
	case 'Q': case 'R': case 'B': case 'N':
		  move[NOTE] = '='; move[PROMOTION] = string[index]++;
		  return choose_move(move, isWhite);
	default: return false;
	}

	switch (string[index]) {
	case '\0': return false;
	case 'Q': case 'R': case 'B': case 'N': move[PROMOTION] = string[index++]; break;
	default: return false;
	}

	if (string[index] != '\0') return false;

	return false;
}
