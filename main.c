/* Robert Higgins
 * Chess
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

#include "Chess_Bitmaps.h"

#define inf_loop for(;;)

enum reason {WIN, DRAW, RESTART};
bool white_turn;
char input[100];

void initialize() {
	Chess_Bitmaps_initialize();
	white_turn = true;
}

void clear_screen(int lines) {
	char emptiness[lines+1];
	for(int a=0;a<lines;a++) emptiness[a] = '\n';
	emptiness[lines] = '\0';
	printf("%s", emptiness);
}

void display_board() {
	clear_screen(40);
	char board[1000];
	Chess_Bitmaps_get_board(board);
	printf("%s\n\n", board);
}

void display_about() {
	clear_screen(40);
	printf("This C_Chess program was created by Robert Higgins as part of a school project\n"
			"All code in this project may be used for other purposes, this project itself\n"
			"cannot be used for commercial purposes.\n\n"
			"Contact information:\n"
			"   Robert Higgins\n"
			"   LinkedIn: https://www.linkedin.com/in/roberthiggins9/\n"
			"   Email:    oldmanjoe9@yahoo.com, RobertHiggins9@yahoo.com\n"
			"             oldmanjoe9@gmail.com, HigginsRobert09@gmail.com\n"
			"   GitHub:   https://github.com/OldManJoe9\n"
			"   GitLab:   https://gitlab.com/OldManJoe\n"
			"      *This project is on GitLab\n\n"
			"Press enter to return to the game.\n");

	fflush(stdout); getchar(); getchar(); display_board();
}

void display_help() {
	clear_screen(40);
	printf("Extra commands:\n"
			"help: prints this message.\n"
			"exit: quits the program.\n"
			"restart: starts a new game.\n"
			"debug: prints out information for sending to the creator.\n"
			"about: prints about information for this game.\n\n"
			"Algebraic notation takes 5 parts.\n"
			"Piece start capture end note\n\n"
			"Piece:   K Q R B N P(optional)\n"
			"start:   only necessary if there are two pieces of the same type that can reach the same square.\n"
			"         this doesn't need to be the full square, can use only the file or rank if that is all that's needed\n"
			"         to differentiate between pieces.\n"
			"capture: if you are capturing a piece you must include an 'x' here, otherwise skip\n"
			"end:     must include the both the file and rank of the end square\n"
			"note:    this can be a + for check, # or ++ for checkmate, these are ignored, what isn't\n"
			"         ignored is '=' for pawn promotion, you can not ignore what piece you want to promote to.\n\n"
			"Example: a4 moves a pawn to a4, cxd4 pawn on c file takes piece on d4\n"
			"         Ndb2 moves the Knight on the d file to b2, N3b2 moves the night on the 3rd rank to b2\n"
			"         Bc3b4 moves the Bishop at c3 to b4, Bc3xb4 the bishop captures the piece at that square\n"
			"         0-0-0 is queen side castle, O-O is king side, zeros or capital 'O's work, but cannot be mixed.\n\n"
			"Press enter to return to the game.\n");

	fflush(stdout); getchar(); getchar(); display_board();
}

//TODO remove debug method
void display_debug_info() {
	clear_screen(40);
	Chess_Bitmaps_print_debug_info();
	printf("white_turn = %s\n\n", white_turn ? "white" : "black");
	display_board();
}

void new_game(int reason) {
	switch (reason) {
	case WIN: printf("%s wins by checkmate! Would you like to play again? [y/n] ", !white_turn ? "White" : "Black"); break;
	case DRAW: printf("Game ends in a Draw. Would you like to play again? [y/n] "); break;
	case RESTART: printf("Are you sure you want to start a new game? [y/n] "); break;
	}

	fflush(stdout); scanf("%s", input);
	if (strcmp(input, "y")) {
		if (reason != RESTART) exit(EXIT_SUCCESS);
		else display_board();
	}
	initialize(); display_board();
}

int main(int argc, char *argv[]) {
	printf("Welcome to Chess in C!\n\n"
			"In order to play you need to understand Algebraic Chess Notation\n"
			"Algebraic Chess Notation requires: \n"
			"\t The piece abbreviation: K, Q, R, B, N, P(optional)\n"
			"\t The starting location if there is more than one piece of the same type that can move\n"
			"\t   to the same location\n"
			"\t If you are capturing a piece you need to put an 'x' before the end location.\n"
			"\t The end location\n"
			"Example: a4 - Pawn move to a4\n"
			"Example: Rcxd4 - The rook on the C file takes the piece on d4\n"
			"\t only one piece of the starting location is necessary unless there are 3 pieces of the\n"
			"\t   same type that must be distinguished between.\n"
			"Castling 0-0-0 or O-O both work\n\n"
			"For extra commands, type 'help'\n\n"
			"Are you ready to play? [y/n] ");
	fflush(stdout); scanf("%s", input);

	if(strcmp(input, "y")) return EXIT_SUCCESS;

	initialize();
	display_board();
	printf("\n");
	bool moved = false;

	inf_loop {
		printf("Enter your move: ");
		fflush(stdout); scanf("%s", input);

		if (!strcmp(input, "exit")) return EXIT_SUCCESS;
		else if (!strcmp(input, "debug")) {display_debug_info(); continue;}
		else if (!strcmp(input, "restart")) {new_game(RESTART); continue;}
		else if (!strcmp(input, "help")) {display_help(); continue;}
		else if (!strcmp(input, "about")) {display_about(); continue;}
		else if (!strcmp(input, "clear")) {Chess_Bitmaps_debug_clear(); display_board(); continue;}
		else if (!strcmp(input, "!")) {white_turn = !white_turn; display_board(); continue;}

		moved = Chess_Bitmaps_move(input, white_turn);
		display_board();
		if (input[0] == '!') {printf("\n"); continue;}

		if (moved) {
			white_turn = !white_turn;
			printf("\n");
			if (Chess_Bitmaps_is_check(white_turn) && Chess_Bitmaps_is_checkmate(white_turn)) new_game(WIN);
			else if (Chess_Bitmaps_is_draw(white_turn)) new_game(DRAW);
		} else printf("Your input was incorrect\n");
	}

	return EXIT_SUCCESS;
}
